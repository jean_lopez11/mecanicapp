import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_meedu/ui.dart';
import 'package:mecanicapp/app/ui/global_controllers/session_controller.dart';
import 'package:mecanicapp/app/ui/pages/stores/stores_page.dart';
import 'package:mecanicapp/app/ui/routes/routes.dart';

class CustomDrawer extends StatefulWidget {
  const CustomDrawer({Key? key}) : super(key: key);

  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 224, 168, 0),
      body: Stack(
        children: [
          Positioned(
            left: 30,
            top: 110,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    const Icon(
                      CupertinoIcons.person_alt,
                      color: Colors.white,
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Consumer(
                      builder: (_, ref, __) {
                        final user = ref.watch(sessionProvider).user!;
                        return Text(
                          user.displayName ?? '',
                          style: const TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Colors.white,
                              fontSize: 25),
                        );
                      },
                    ),
                  ],
                ),
                const Divider(
                  color: Colors.white,
                  thickness: 1,
                  height: 40,
                ),
                Positioned(
                    bottom: 50,
                    left: 30,
                    child: Row(
                      children: <Widget>[
                        const Icon(
                          CupertinoIcons.exclamationmark_triangle,
                          color: Colors.white,
                        ),
                            
                        const SizedBox(
                          width: 15,
                        ),
                        GestureDetector(
                            child: const Text(
                              'Espacio 1',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 17),
                            ),
                            onTap: () {}),
                        const SizedBox(
                          width: 15,
                        ),
                      ],
                    )),
                const Divider(
                  color: Colors.white,
                  thickness: 1,
                  height: 40,
                ),
                Positioned(
                    bottom: 50,
                    left: 30,
                    child: Row(
                      children: <Widget>[
                        const Icon(
                            CupertinoIcons.exclamationmark_square,
                            color: Colors.white),
                        const SizedBox(
                          width: 15,
                        ),
                        GestureDetector(
                            child: const Text(
                              "Espacio 2",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 17),
                            ),
                            onTap: () {}),
                        const SizedBox(
                          width: 15,
                        ),
                      ],
                    )),
                const Divider(
                  color: Colors.white,
                  thickness: 1,
                  height: 40,
                ),
                Positioned(
                    bottom: 50,
                    left: 30,
                    child: Row(
                      children: <Widget>[
                        const Icon(
                            CupertinoIcons.person_2,
                            color: Colors.white),
                        const SizedBox(
                          width: 15,
                        ),
                        GestureDetector(
                            child: const Text(
                              "Contactos",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 17),
                            ),
                            onTap: () {}),
                        const SizedBox(
                          width: 15,
                        ),
                      ],
                    )),
                const Divider(
                  color: Colors.white,
                  thickness: 1,
                  height: 40,
                ),
                Positioned(
                    bottom: 50,
                    left: 30,
                    child: Row(
                      children: <Widget>[
                        const Icon(
                            CupertinoIcons.exclamationmark_shield,
                            color: Colors.white),
                        const SizedBox(
                          width: 15,
                        ),
                        GestureDetector(
                            child: const Text(
                              "Acerca de",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 17),
                            ),
                            onTap: () {}),
                        const SizedBox(
                          width: 15,
                        ),
                      ],
                    )),
              ],
            ),
          ),
          Positioned(
              bottom: 50,
              left: 30,
              child: Row(
                children: <Widget>[
                  GestureDetector(
                    child: const Text(
                      'Cerrar Sesión',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 21),
                    ),
                    onTap: () async {
                      await sessionProvider.read.signOut();
                      router.pushNamedAndRemoveUntil(Routes.LOGIN);
                    },
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  const ImageIcon(AssetImage("lib/assets/signoutarrow.png"),
                      color: Colors.white),
                ],
              ))
        ],
      ),
    );
  }
}
