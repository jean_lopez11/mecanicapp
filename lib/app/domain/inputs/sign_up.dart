class SignUpData{
  final String name, surname, email, password;

  SignUpData({
    required this.name,
    required this.surname,
    required this.email,
    required this.password});
}

