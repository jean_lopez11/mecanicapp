import 'package:mecanicapp/app/domain/inputs/sign_up.dart';
import 'package:mecanicapp/app/domain/responses/sign_up_response.dart';

abstract class SignUpRepository{

  Future <SignUpResponse> register(SignUpData data);

}


