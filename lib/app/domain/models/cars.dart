import 'dart:convert';

Car carFromJson(String str) => Car.fromJson(json.decode(str));

String carToJson(Car data) => json.encode(data.toJson());

class Car {
    Car({
        this.id,
        required this.modelo,
        required this.anio,
        required this.placa,
    });

    String? id;
    final String modelo;
    final String anio;
    final String placa;

    factory Car.fromJson(Map<String, dynamic> json) => Car(
        id: json["id"],
        modelo: json["modelo"],
        anio: json["anio"],
        placa: json["placa"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "modelo": modelo,
        "anio": anio,
        "placa": placa,
    };
}