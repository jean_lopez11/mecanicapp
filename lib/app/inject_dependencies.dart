import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_meedu/meedu.dart';
import 'package:mecanicapp/app/data/repositories_impl/account_repository_impl.dart';
import 'package:mecanicapp/app/data/repositories_impl/authentication_repository_impl.dart';
import 'package:mecanicapp/app/data/repositories_impl/sign_up_repository_impl.dart';
import 'package:mecanicapp/app/domain/repositories/account_repository.dart';
import 'package:mecanicapp/app/domain/repositories/authentication_respository.dart';
import 'package:mecanicapp/app/domain/repositories/sign_up_repository.dart';

void injectDependencies() {
  Get.lazyPut<AuthenticationRepository>(
    () => AuthenticationRepositoryImpl(FirebaseAuth.instance),
  );
  Get.lazyPut<SignUpRepository>(
    () => SignUpRepositoryImpl(FirebaseAuth.instance),
  );
  Get.lazyPut<AccountRepository>(
    () => AccountRepositoryImpl(
      FirebaseAuth.instance,
    ),
  );
}
