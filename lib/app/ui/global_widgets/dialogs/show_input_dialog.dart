import 'package:flutter/cupertino.dart';

Future<String?> showInputDialog(
  BuildContext context, {
  String? title,
  String? initialValue,
}) async {
  String value = initialValue ?? '';
  TextEditingController controller = TextEditingController();
  controller.text = value;
  final result = await showCupertinoDialog<String>(
    context: context,
    builder: (context) => CupertinoAlertDialog(
      title: title != null ? Text(title) : null,
      //title: Text(""),
      content: CupertinoTextField(
        controller: controller,
        onChanged: (text) {
        value = text;
      },
      ),
      actions: [
        CupertinoDialogAction(
          onPressed: () {
            Navigator.pop(context,value);
          },
          isDefaultAction: true,
          child: const Text("Guardar"),
          ),
          CupertinoDialogAction(
          onPressed: () {
            Navigator.pop(context);
          },
          isDestructiveAction: true,
          child: const Text("Cancelar"),
          ),
          
      ],
    ),
  );

  controller.dispose();

  if(result != null && result.trim().isEmpty){
    return null;
  }

  return result;
}
