import 'dart:ffi';

import 'package:flutter_meedu/meedu.dart';
import 'package:mecanicapp/app/domain/repositories/authentication_respository.dart';
import 'package:mecanicapp/app/ui/global_controllers/session_controller.dart';
import 'package:mecanicapp/app/ui/routes/routes.dart';

class SplashController extends SimpleNotifier {
  final SessionController _sessionController;
  final AuthenticationRepository _authRepository = Get.find();

  String? _routeName;
  String? get routeName => _routeName;
  String admin ="";

  SplashController(this._sessionController) {
    _init();
  }

  void _init() async {
    //getProducts();
    final user = await _authRepository.user;
    if (user != null) {
      //print(user);


      
      // _routeName = Routes.HOME;
       _sessionController.setUser(user);
             admin = _sessionController.user!.displayName.toString();

      print("Usuario Logeado: "+ admin);
      if (admin.toString() != 'Usuario Admin') {
        _routeName = Routes.HOME; 
      }else{
        
      }
      
      //print(_sessionController.user!.displayName);
    } else {
      _routeName = Routes.LOGIN;
    }
    notify();
  }
}
