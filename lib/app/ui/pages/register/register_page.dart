import 'package:flutter_meedu/ui.dart';
import 'package:mecanicapp/app/ui/global_controllers/session_controller.dart';
import 'package:mecanicapp/app/ui/global_widgets/custom_input_field.dart';
import 'package:mecanicapp/app/ui/pages/register/controller/register_controller.dart';
import 'package:mecanicapp/app/ui/pages/register/controller/register_state.dart';
import 'package:mecanicapp/app/ui/pages/register/utils/send_register_form.dart';
import 'package:mecanicapp/app/utils/email_validator.dart';
import 'package:mecanicapp/app/utils/name_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_meedu/meedu.dart';
import 'package:mecanicapp/app/utils/send_button.dart';

final registerProvider = StateProvider<RegisterController, RegisterState>(
  (_) => RegisterController(sessionProvider.read),
);

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    return ProviderListener<RegisterController>(
      provider: registerProvider,
      builder: (_, controller) {
        return Scaffold(
          backgroundColor: const Color(0xffF2F2F2),
          appBar: AppBar(
            automaticallyImplyLeading: false,
            elevation: 0,
            backgroundColor: const Color(0xffF2F2F2),
            title: Row(
              children: <Widget>[
                SizedBox(
                  height: 40,
                  width: 40,
                  child: IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: const ImageIcon(
                      AssetImage("lib/assets/appbar_back.png"),
                      color: Colors.black,
                    ),
                  ),
                ),
              ],
            ),
          ),
          body: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Container(
                width: double.infinity,
                height: double.infinity,
                color: Colors.transparent,
                child: Form(
                  key: controller.formKey,
                  child: ListView(
                    padding: const EdgeInsets.all(15),
                    children: [
                      const Text(
                        "Registro de Usuario",
                        style: TextStyle(
                            fontSize: 34, fontWeight: FontWeight.w700),
                        //textAlign: TextAlign.center,
                      ),
                      const SizedBox(
                        height: 50,
                      ),
                      CustomInputField(
                        label: "Nombre",
                        onChanged: controller.onNameChanged,
                        validator: (text) {
                          return isValidName(text!) ? null : "Dato invalido";
                        },
                      ),
                      const SizedBox(
                        height: 25,
                      ),
                      CustomInputField(
                        label: "Apellido",
                        onChanged: controller.onSurNameChanged,
                        validator: (text) {
                          return isValidName(text!) ? null : "Dato invalido";
                        },
                      ),
                      const SizedBox(
                        height: 25,
                      ),
                      CustomInputField(
                        label: "Correo Electrónico",
                        inputType: TextInputType.emailAddress,
                        onChanged: controller.onEmailChanged,
                        validator: (text) {
                          return isValidEmail(text!)
                              ? null
                              : "Correo electronico invalido";
                        },
                      ),
                      const SizedBox(
                        height: 25,
                      ),
                      CustomInputField(
                        label: "Contraseña",
                        isPassword: true,
                        onChanged: controller.onPasswordChanged,
                        validator: (text) {
                          if (text!.trim().length >= 5) {
                            return null;
                          }
                          return "Contraseña invalida";
                        },
                      ),
                      const SizedBox(
                        height: 25,
                      ),
                      Consumer(builder: (_, ref, __) {
                        final controller = ref
                            .watch(registerProvider.select((_) => _.password));
                        return CustomInputField(
                          label: "Repite la Contraseña",
                          isPassword: true,
                          onChanged: controller.onVPasswordChanged,
                          validator: (text) {
                            if (controller.state.password != text) {
                              return "Las contraseñas no coinciden";
                            }
                            if (text!.trim().length >= 5) {
                              return null;
                            }
                            return "invalid password";
                          },
                        );
                      }),
                      const SizedBox(
                        height: 30,
                      ),
                      Row(
                        //crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            // elevation: 6.0,
                            // shape: RoundedRectangleBorder(
                            //   borderRadius: BorderRadius.circular(10),
                            // ),
                            // color: Colors.amber,
                            style: raisedButtonStyle,
                            onPressed: () => sendRegisterForm(context),
                            child: const Text(
                              "Crear Cuenta",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                )),
          ),
        );
      },
    );
  }
}
