import 'package:flutter/cupertino.dart';
import 'package:flutter_meedu/ui.dart';
import 'package:mecanicapp/app/domain/responses/sign_up_response.dart';
import 'package:mecanicapp/app/ui/global_widgets/dialogs/dialogs.dart';
import 'package:mecanicapp/app/ui/global_widgets/dialogs/progress_dialog.dart';
import 'package:mecanicapp/app/ui/pages/register/register_page.dart';
import 'package:mecanicapp/app/ui/routes/routes.dart';


Future <void> sendRegisterForm(BuildContext context) async {
  final controller = registerProvider.read;
  final isValidForm = controller.formKey.currentState!.validate();

  if(isValidForm){
    ProgressDiaog.show(context);
    final response = await controller.submit();
    router.pop();
    
    if (response.error != null){
      print("FATAL error ${response.error}");
      String content = "";

      switch(response.error){
      
        case SignUpError.emailAlreadyInUse:
          content = "Correo electrónico ya en uso";
          break;
        case SignUpError.weakPassword:
          content = "Contraseña débil";
          break;
        case SignUpError.networkRequestFailed:
          content = "Sin conexion a internet";
          break;
        case SignUpError.tooManyRequests:
          content = "Intente más tarde";
          break;          
        case SignUpError.unknown:
        default:
          content = "Error desconocido";
          break;
    }
      Dialogs.alert(
        context,
        title: "ERROR",
        content: content,
        );
    }else{
      print("Redireccionand a Home");
      router.pushNamedAndRemoveUntil(
        Routes.HOME
        );
    }

  }else{
    Dialogs.alert(
      context,
      title: "ERROR",
      content: "Campos inválidos");
  }
}