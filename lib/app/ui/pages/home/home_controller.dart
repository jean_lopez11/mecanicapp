


import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_meedu/meedu.dart';

class HomeController extends SimpleNotifier implements TickerProvider{

  late TabController tabController;
  HomeController() {
    tabController = TabController(
      length: 4, 
      vsync: this,
      );
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }
  @override
  Ticker createTicker(TickerCallback onTick) => Ticker(onTick);
}