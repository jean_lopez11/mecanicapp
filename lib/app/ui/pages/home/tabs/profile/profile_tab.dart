import 'package:flutter/material.dart';
import 'package:flutter_meedu/ui.dart';
import 'package:mecanicapp/app/ui/global_controllers/session_controller.dart';
import 'package:mecanicapp/app/ui/global_widgets/dialogs/dialogs.dart';
import 'package:mecanicapp/app/ui/global_widgets/dialogs/progress_dialog.dart';
import 'package:mecanicapp/app/ui/global_widgets/dialogs/show_input_dialog.dart';

class ProfileTab extends ConsumerWidget{
  const ProfileTab({Key? key}) : super(key: key);


  void _updateDisplayName(BuildContext context) async{
    final sessionController = sessionProvider.read;
    final value = await showInputDialog(
      context,
      initialValue: sessionController.user!.displayName??"",
      );
    if (value != null) {
      ProgressDiaog.show(context);
      final user = await sessionProvider.read.updateDisplayName(value);
      Navigator.pop(context);
      if (user == null) {
        Dialogs.alert(
          context, 
          title: "Error",
          content: "Sin acceso a internet",
          );
      }
    }
    print("datossss $value");
  }

  @override
  Widget build(BuildContext context, ref) {
    final sessionController = ref.watch(sessionProvider);
    final user = sessionController.user!;

    final displayName = user.displayName ?? '';
    final letter = displayName.isNotEmpty? displayName[0] : "";

    return ListView(
      //padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      children: [
        const SizedBox( height: 60,),
        CircleAvatar(
          backgroundColor: Colors.amber,
          radius: 75,
          backgroundImage: user.photoURL!=null?NetworkImage(user.photoURL!):null,
          child: user.photoURL == null? Text(
            letter, 
            style: const TextStyle(
              fontSize: 65,
              color: Colors.black),
              )
              :null,
        ),
        const SizedBox(height: 10,),
        Center(child: Text(displayName, style: const TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.bold,
        ),)),
        Center(child: Text(user.email?? '')),

        const SizedBox(height: 50),
        //const Text("Datos del usuario"),
        LabelButton(
          label: "Nombre", 
          value: displayName,
          onPressed: () => _updateDisplayName(context),
          ),
        LabelButton(
          label: "Correo Electronico", 
          value: user.email ?? '',
          ),
        LabelButton(
          label: "Usuario Verificado", 
          value: user.emailVerified?"Si":"No",
          ),
      ],
    );
  }
}


class LabelButton extends StatelessWidget {
  final String label, value;
    final VoidCallback? onPressed;
    
  const LabelButton({Key? key, 
  required this.label, 
  required this.value, 
  this.onPressed,
  }) : super(key: key);

    
  @override
  Widget build(BuildContext context) {
    
    

    return ListTile(
      onTap: onPressed,
          contentPadding: const EdgeInsets.symmetric(vertical: 0, horizontal: 20),
          leading: Text(
            label,
            style: const TextStyle(fontWeight: FontWeight.w500),),
          trailing: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(value, style: const TextStyle(
                fontWeight: FontWeight.w300,
              ),),
              const SizedBox(width: 5,),
              Icon(Icons.chevron_right_outlined, 
              size: 22,
              color: onPressed!=null?Colors.black45:Colors.transparent),
            ],
          ),
        );
  }
}