import 'package:flutter/material.dart';
import 'package:mecanicapp/app/ui/pages/vehicle/vehicle_page.dart';
import 'package:mecanicapp/app/utils/send_button.dart';
import 'package:mecanicapp/widgets/drawer_widget.dart';

class HomeTab extends StatefulWidget {
  const HomeTab({Key? key}) : super(key: key);

  @override
  _HomeTabState createState() => _HomeTabState();
}

var scaffoldkey = GlobalKey<ScaffoldState>();

class _HomeTabState extends State<HomeTab> with SingleTickerProviderStateMixin {
  TabController? _tabController;
  

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldkey,
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color(0xffF2F2F2),
      drawer: const CustomDrawer(),
      appBar: AppBar(
        titleSpacing: 0.0,
        backgroundColor: const Color(0xffF2F2F2),
        elevation: 0.0,
        leading: Padding(
          padding: const EdgeInsets.only(left: 20.0),
          child: IconButton(
              color: Colors.black,
              icon: const ImageIcon(AssetImage("lib/assets/drawer.png")),
              onPressed: () => scaffoldkey.currentState?.openDrawer()), //
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.only(left: 30.0, right: 30.0, top: 10),
            child: SizedBox(
              width: 200,
              child: Text(
                "Registro de Vehiculo",
                style: TextStyle(fontSize: 34, fontWeight: FontWeight.w700),
                textAlign: TextAlign.start,
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          ElevatedButton(
              style: raisedButtonStyle,
              
              
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => VehiclePage()));
              },
              child: const Text(
                "Añadir vehiculo",
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.black,
                ),
              )),
        ],
      ),
    );
  }
}
