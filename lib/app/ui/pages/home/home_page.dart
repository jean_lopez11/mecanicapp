import 'package:flutter/material.dart';
import 'package:flutter_meedu/meedu.dart';
import 'package:flutter_meedu/ui.dart';
import 'package:mecanicapp/app/ui/pages/home/home_controller.dart';
import 'package:mecanicapp/app/ui/pages/home/tabs/home/home_tab.dart';
import 'package:mecanicapp/app/ui/pages/home/tabs/profile/profile_tab.dart';
import 'package:mecanicapp/app/ui/pages/home/widgets/home_tab_bar.dart';
import 'package:mecanicapp/app/ui/pages/one/one_page.dart';
import 'package:mecanicapp/app/ui/pages/two/two_page.dart';

final homeProvider = SimpleProvider(
  (_) => HomeController(),
);


class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return ProviderListener<HomeController>(
      provider: homeProvider,
      builder: (_, controller){
        return Scaffold(
          bottomNavigationBar: HomeTabBar(),
          body: SafeArea(
            child:  TabBarView(
              controller: controller.tabController,
              children: const [
            
            HomeTab(),
            // OrderTab(),
            //Text("Hola"),
            OnePage(),
            TwoPage(),
            //Text("Hola"),
            //ProfileTab(),
            ProfileTab(),
          ]),
        )
    );
      },
    );

  }


}