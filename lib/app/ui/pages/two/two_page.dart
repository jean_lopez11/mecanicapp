import 'dart:convert';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';


class TwoPage extends StatefulWidget {
  const TwoPage({Key? key}) : super(key: key);

  @override
  _TwoPageState createState() => _TwoPageState();
}

class _TwoPageState extends State<TwoPage> {
  @override
  void initState() {
    super.initState();
    _activateListeners();
  }

  void _activateListeners() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      appBar: UsualAppBar(
        context,
        "Espacio 2",
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 10.0, bottom: 30.0),
        child: Column(
          children: const [],
        ),
      ),
    );
  }
}

PreferredSizeWidget UsualAppBar(BuildContext context, String title) => AppBar(
      elevation: 0,
      centerTitle: true,
      backgroundColor: Color(0xffF2F2F2),
      title: Text(
        title,
        style: TextStyle(color: Colors.black, fontSize: 18),
      ),
    );
