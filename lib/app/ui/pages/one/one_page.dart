import 'dart:convert';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';


class OnePage extends StatefulWidget {
  const OnePage({Key? key}) : super(key: key);

  @override
  _OnePageState createState() => _OnePageState();
}

class _OnePageState extends State<OnePage> {
  @override
  void initState() {
    super.initState();
    _activateListeners();
  }

  void _activateListeners() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      appBar: UsualAppBar(
        context,
        "Espacio 1",
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 10.0, bottom: 30.0),
        child: Column(
          children: const [],
        ),
      ),
    );
  }
}

PreferredSizeWidget UsualAppBar(BuildContext context, String title) => AppBar(
      elevation: 0,
      centerTitle: true,
      backgroundColor: Color(0xffF2F2F2),
      title: Text(
        title,
        style: TextStyle(color: Colors.black, fontSize: 18),
      ),
    );
