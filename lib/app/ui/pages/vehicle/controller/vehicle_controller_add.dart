import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:mecanicapp/app/domain/models/cars.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mecanicapp/app/ui/pages/vehicle/vehicle_page.dart';
import 'package:mecanicapp/app/utils/send_button.dart';

class Vehicle_Controller_Add extends StatefulWidget {
  Vehicle_Controller_Add({Key? key}) : super(key: key);

  @override
  State<Vehicle_Controller_Add> createState() => _Vehicle_Controller_AddState();
}

class _Vehicle_Controller_AddState extends State<Vehicle_Controller_Add> {
  final TextEditingController modeloController = TextEditingController();
  final TextEditingController anioController = TextEditingController();
  final TextEditingController placaController = TextEditingController();
  final FocusNode focusNode = FocusNode();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: UsualAppBar(
        context,
        "Añadir vehículos",
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            getMyField(
                focusNode: focusNode,
                hintText: 'Modelo',
                controller: modeloController),
            getMyField(
                hintText: 'Año',
                textInputType: TextInputType.number,
                controller: anioController),
            getMyField(hintText: 'Placa', controller: placaController),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  style: raisedButtonStyle,
                  onPressed: () {
                    Car car = Car(
                      modelo: modeloController.text,
                      anio: anioController.text,
                      placa: placaController.text,
                    );
                    addCarAndNavigateToHome(car, context);
                  },
                  child: const Text('Añadir'),
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.red,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10))),
                  onPressed: () {
                    modeloController.text = '';
                    anioController.text = '';
                    placaController.text = '';
                    focusNode.requestFocus();
                  },
                  child: const Text('Limpiar'),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget getMyField(
      {required String hintText,
      TextInputType textInputType = TextInputType.name,
      required TextEditingController controller,
      FocusNode? focusNode}) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: TextField(
        focusNode: focusNode,
        controller: controller,
        keyboardType: textInputType,
        decoration: InputDecoration(
            hintText: 'Ingrese $hintText',
            labelText: hintText,
            border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(5)))),
      ),
    );
  }

  void addCarAndNavigateToHome(Car car, BuildContext context) {
    final carRef = FirebaseFirestore.instance.collection('cars').doc();
    car.id = carRef.id;
    final data = car.toJson();
    carRef.set(data).whenComplete(() {
      log('Se cargaron los datos');
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => VehiclePage(),
          ));
    });
  }
}

PreferredSizeWidget UsualAppBar(BuildContext context, String title) => AppBar(
      elevation: 0,
      centerTitle: true,
      backgroundColor: Colors.amber,
      title: Text(
        title,
        style: const TextStyle(color: Colors.black, fontSize: 18),
      ),
    );
