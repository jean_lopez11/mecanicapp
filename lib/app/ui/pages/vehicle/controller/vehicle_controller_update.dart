import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:mecanicapp/app/domain/models/cars.dart';
import 'package:mecanicapp/app/ui/pages/vehicle/vehicle_page.dart';
import 'package:mecanicapp/app/utils/send_button.dart';

class Vehicle_Controller_Update extends StatelessWidget {
  
  final Car car;

  final TextEditingController modeloController = TextEditingController();

  final TextEditingController anioController = TextEditingController();

  final TextEditingController placaController = TextEditingController();

  final FocusNode focusNode = FocusNode();

  
  Vehicle_Controller_Update({super.key, required this.car});
  
  @override
  Widget build(BuildContext context) {
    modeloController.text = '${car.modelo}';
    anioController.text = car.anio;
    placaController.text = '${car.placa}';
    return Scaffold(
      appBar: UsualAppBar(
        context,
        "Editar vehículo",
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(height: 20,),
            getMyField(
              focusNode: focusNode,
              hintText: 'Modelo', controller: modeloController),
            getMyField(
                hintText: 'Año',
                textInputType: TextInputType.number,
                controller: anioController),
            getMyField(hintText: 'Placa', controller: placaController),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  style: raisedButtonStyle,
                  onPressed: () {
                    Car updateCar = Car(
                      id: car.id,
                      modelo: modeloController.text,
                      anio: anioController.text,
                      placa: placaController.text);
                      final collectionReference = 
                      FirebaseFirestore.instance.collection('cars');
                      collectionReference
                      .doc(updateCar.id)
                      .update(updateCar.toJson())
                      .whenComplete((){
                        log('Car Update');
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => VehiclePage(),)
                          );
                      });
                  },
                  child: const Text('Actualizar'),
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.red,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))
                    ),
                  onPressed: () {
                    // modeloController.text = '';
                    // anioController.text = '';
                    // placaController.text = '';
                    // focusNode.requestFocus();
                    Navigator.pop(context); 
                  },
                  child: const Text('Volver'),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget getMyField(
      {required String hintText,
      TextInputType textInputType = TextInputType.name,
      required TextEditingController controller, FocusNode? focusNode}) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: TextField(
        focusNode: focusNode,
        controller: controller,
        keyboardType: textInputType,
        decoration: InputDecoration(
            hintText: 'Ingrese $hintText',
            labelText: hintText,
            border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(5)))),
      ),
    );
  }
}

PreferredSizeWidget UsualAppBar(BuildContext context, String title) => AppBar(
      elevation: 0,
      centerTitle: true,
      backgroundColor: Colors.amber,
      title: Text(
        title,
        style: TextStyle(color: Colors.black, fontSize: 18),
      ),
    );
