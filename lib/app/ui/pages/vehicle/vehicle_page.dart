
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:mecanicapp/app/domain/models/cars.dart';
import 'package:mecanicapp/app/ui/pages/vehicle/controller/vehicle_controller_add.dart';
import 'package:mecanicapp/app/ui/pages/vehicle/controller/vehicle_controller_update.dart';

class VehiclePage extends StatelessWidget {
  final CollectionReference _reference =
      FirebaseFirestore.instance.collection('cars');

  VehiclePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      appBar: UsualAppBar(
        context,
        "Vehículos",
      ),
      body: FutureBuilder<QuerySnapshot>(
        future: _reference.get(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Center(
              child: Text('Something went wrong'),
            );
          }
          if (snapshot.hasData) {
            QuerySnapshot querySnapshot = snapshot.data!;
            List<QueryDocumentSnapshot> documents = querySnapshot.docs;
            List<Car> cars = documents
                .map((e) => Car(
                    id: e['id'],
                    modelo: e['modelo'],
                    anio: e['anio'],
                    placa: e['placa']))
                .toList();
            return _getCar(cars);
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
        // child: _getCar()
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.cyan,
        onPressed: (() {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => Vehicle_Controller_Add(),
              ));
        }),
        child: const Icon(Icons.add),
      ),
    );
  }

  Widget _getCar(cars) {
    return cars.isEmpty
        ? const Center(
            child: Text('Vacio'),
          )
        : ListView.builder(
            itemCount: cars.length,
            itemBuilder: (context, index) => Card(
              child: ListTile(
                title: Text(cars[index].modelo),
                subtitle: Text('Placa: ${cars[index].placa}'),
                leading: CircleAvatar(
                  radius: 25,
                  backgroundColor: Colors.amber,
                  child: Text(
                    cars[index].anio,
                    style: const TextStyle(color: Colors.black),
                  ),
                ),
                trailing: SizedBox(
                  width: 60,
                  child: Row(
                    children: [
                      InkWell(
                        child: Icon(
                          Icons.edit,
                          color: Colors.black.withOpacity(0.75),
                        ),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    Vehicle_Controller_Update(car: cars[index]),
                              ));
                        },
                      ),
                      InkWell(
                        child: Icon(
                          Icons.delete,
                          color: Colors.red.withOpacity(0.75),
                        ),
                        onTap: () {
                          _reference.doc(cars[index].id).delete();
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => VehiclePage()));
                        },
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
  }

  PreferredSizeWidget UsualAppBar(BuildContext context, String title) => AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.amber,
        title: Text(
          title,
          style: TextStyle(color: Colors.black, fontSize: 18),
        ),
      );
}
