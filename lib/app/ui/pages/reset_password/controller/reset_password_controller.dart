import 'package:flutter_meedu/meedu.dart';
import 'package:mecanicapp/app/domain/repositories/authentication_respository.dart';
import 'package:mecanicapp/app/domain/responses/reset_password_response.dart';

class ResetPasswordController extends SimpleNotifier{
  String _email='';
  String get email => _email;

  final _authenticationRepository = Get.find<AuthenticationRepository>();

  void onEmailChanged(String text){
    _email = text;
  }
  Future <ResetPasswordResponse> submit(){
    return _authenticationRepository.sendResetPasswordLink(email);
  }
}