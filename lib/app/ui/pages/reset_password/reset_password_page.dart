import 'package:flutter/material.dart';
import 'package:flutter_meedu/meedu.dart';
import 'package:flutter_meedu/ui.dart';
import 'package:mecanicapp/app/domain/responses/reset_password_response.dart';
import 'package:mecanicapp/app/ui/global_widgets/custom_input_field.dart';
import 'package:mecanicapp/app/ui/global_widgets/dialogs/progress_dialog.dart';
import 'package:mecanicapp/app/ui/pages/reset_password/controller/reset_password_controller.dart';
import 'package:mecanicapp/app/utils/send_button.dart';

import '../../../utils/email_validator.dart';
import '../../global_widgets/dialogs/dialogs.dart';

final resetPasswordProvider = SimpleProvider(
  (_) => ResetPasswordController(),
);

class ResetPasswordPage extends StatelessWidget {
  const ResetPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ProviderListener<ResetPasswordController>(
      provider: resetPasswordProvider,
      builder: (_, controller) => Scaffold(
        backgroundColor: const Color(0xffF2F2F2),
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          backgroundColor: const Color(0xffF2F2F2),
          title: Row(
            children: <Widget>[
              SizedBox(
                height: 40,
                width: 40,
                child: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const ImageIcon(
                    AssetImage("lib/assets/appbar_back.png"),
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          ),
        ),
        body: SafeArea(
            child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Container(
            width: double.infinity,
            color: Colors.transparent,
            padding: const EdgeInsets.all(15),
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.end,
              children: [
                const Text(
                  "Recuperación de Cuenta",
                  style: TextStyle(fontSize: 34, fontWeight: FontWeight.w700),
                  //textAlign: TextAlign.center,
                ),
                const SizedBox(
                  height: 50,
                ),
                CustomInputField(
                  label: "Correo",
                  onChanged: controller.onEmailChanged,
                  inputType: TextInputType.emailAddress,
                ),
                const SizedBox(
                  height: 30,
                ),


                ElevatedButton(
                    // elevation: 6.0,
                    // shape: RoundedRectangleBorder(
                    //   borderRadius: BorderRadius.circular(10),
                    // ),
                    // color: Colors.amber,
                    style: raisedButtonStyle,

                    onPressed: () {
                      
                    },
                    child: const Text(
                      "Enviar",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                      ),
                    )),
              ],
            ),
          ),
        )),
      ),
    );
  }

  void _sumbit(BuildContext context) async {
    final controller = resetPasswordProvider.read;
    if (isValidEmail(controller.email)) {
      ProgressDiaog.show(context);
      final response = await controller.submit();
      Navigator.pop(context);

      print(response.toString());
      print(controller.email);

      if (response == ResetPasswordResponse.ok) {
        Dialogs.alert(context,
            title: "Hecho", content: "Correo electronico enviado");
      } else {
        String errorMessage = "";

        switch (response) {
          case ResetPasswordResponse.networkRequestFailed:
            errorMessage = "Sin conexion a internet";
            break;
          case ResetPasswordResponse.userDisabled:
            errorMessage = "Usuario desabilitado";
            break;
          case ResetPasswordResponse.userNotFound:
            errorMessage = "Usuario no encontrado";
            break;
          case ResetPasswordResponse.tooManyRequest:
            errorMessage = "intente mas tarde";
            break;
          case ResetPasswordResponse.unknown:

          default:
            errorMessage = "Error Desconocido";
            // TODO: Handle this case.
            break;
        }

        Dialogs.alert(
          context,
          title: "ERROR",
          content: errorMessage,
        );
      }
    } else {
      Dialogs.alert(context, content: "Correo electronico no valido");
    }
  }
}
