
import 'package:flutter/material.dart';
import 'package:flutter_meedu/meedu.dart';
import 'package:flutter_meedu/ui.dart';
import 'package:mecanicapp/app/ui/global_controllers/session_controller.dart';
import 'package:mecanicapp/app/ui/global_widgets/custom_input_field.dart';
import 'package:mecanicapp/app/utils/send_button.dart';
import 'package:mecanicapp/app/ui/pages/login/utils/send_login_form.dart';
import 'package:mecanicapp/app/ui/pages/reset_password/reset_password_page.dart';
import 'package:mecanicapp/app/utils/email_validator.dart';

import '../register/register_page.dart';
import 'controller/login_controller.dart';

final loginProvider = SimpleProvider(
  (_) => LoginController(sessionProvider.read),
);

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _keyForm = GlobalKey<FormState>();
  //final _controller = loginProvider.read;

  @override
  Widget build(BuildContext context) {
    return ProviderListener<LoginController>(
      provider: loginProvider,
      builder: (_, controller) {
        
        return Scaffold(
          body: SafeArea(
            child: GestureDetector(
              onTap: () => FocusScope.of(context).unfocus(),
              child: Container(
                color: Colors.transparent,
                width: double.infinity,
                padding: const EdgeInsets.all(15),
                child: Form(
                  key: controller.formKey,
                  child: Column(
                    //mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,

                    children: [
                      const SizedBox(
                        height: 50.0,
                      ),
                      const Flexible(
                        child: Text(
                          'Iniciar Sesión',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 40.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      Flexible(
                        child: Image.asset(
                          'images/logo.png',
                          height: 220.0,
                        ),
                      ),
                      const SizedBox(
                        height: 15.0,
                      ),

                      CustomInputField(
                        label: "Correo Electrónico",
                        onChanged: controller.onEmailChanged,
                        inputType: TextInputType.emailAddress,
                        validator: (text) {
                          if (isValidEmail(text!)) {
                            return null;
                          }
                          return "Correo electrónico invalido";
                        },
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      CustomInputField(
                        label: "Contraseña",
                        onChanged: controller.onPasswordChanged,
                        isPassword: true,
                        validator: (text) {
                          if (text!.trim().length >= 5) {
                            return null;
                          }
                          return "Contraseña Incorrecta";
                        },
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      ElevatedButton(
                          // style: ButtonStyle(
                          //   elevation: 10,
                          //   backgroundColor:
                          //       MaterialStateProperty.all(Colors.amber),

                          // ),
                          // elevation: 5.0,
                          // color: Colors.amber,
                          // shape: RoundedRectangleBorder(
                          //   borderRadius: BorderRadius.circular(10),
                          // ),
                          style: raisedButtonStyle,
                          
                          onPressed: () => sendLoginForm(context),
                          child: const Text(
                            "Iniciar Sesión",
                            style: TextStyle(fontSize: 16, color: Colors.black),
                          )),
                      const SizedBox(
                        height: 6,
                      ),
                      GestureDetector(
                            child: const Text(
                              '¿Olvido su contraseña?',
                              style: TextStyle(
                                  fontSize: 17,
                                  color: Color.fromARGB(255, 74, 159, 255),
                                  fontWeight: FontWeight.bold),
                            ),
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    const ResetPasswordPage(),
                              ));
                            },
                          ),
                          const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          const Text(
                            '¿Eres Nuevo?',
                            style: TextStyle(
                              fontSize: 17,
                              color: Color.fromARGB(255, 74, 159, 255),
                            ),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          GestureDetector(
                            child: const Text(
                              'Crea una cuenta',
                              style: TextStyle(
                                  fontSize: 17,
                                  color: Color.fromARGB(255, 74, 159, 255),
                                  fontWeight: FontWeight.bold),
                            ),
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    const RegisterPage(),
                              ));
                            },
                          ),
                          
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
