
import 'dart:math';


import 'package:flutter/cupertino.dart';
import 'package:flutter_meedu/ui.dart';
import 'package:mecanicapp/app/domain/responses/sign_in_responses.dart';
import 'package:mecanicapp/app/ui/global_widgets/dialogs/dialogs.dart';
import 'package:mecanicapp/app/ui/global_widgets/dialogs/progress_dialog.dart';
import 'package:mecanicapp/app/ui/pages/login/login_page.dart';
import 'package:mecanicapp/app/ui/routes/routes.dart';



Future<void> sendLoginForm(BuildContext context) async{
  final controller = loginProvider.read;
  final isValidForm = controller.formKey.currentState!.validate();



  if (isValidForm) {
    ProgressDiaog.show(context);
    final response = await controller.submit(e);
    router.pop();
    if (response.error!=null) {
      print("${response.error}");
      String errorMessage = "";

      switch(response.error){
        
        case SignInError.networkRequestFailed:
          errorMessage = "Sin Acceso a Internet";
          break;
        case SignInError.userDisabled:
          errorMessage = "Su cuenta esta desabilitada";
          break;
        case SignInError.userNotFound:
          errorMessage = "Esta cuenta no existe";
          break;
        case SignInError.wrongPassword:
          errorMessage = "Contraseña incorrecta";
          break;
        case SignInError.tooManyRequests:
          errorMessage = "Intentelo mas tarde";
          break;  
        case SignInError.unknown:
        default:
          errorMessage = "Error desconocido";
          break;
      }

      Dialogs.alert(
        context, 
        title: "ERROR",
        content: errorMessage,
        );

    }else{
      router.pushReplacementNamed(Routes.HOME);
    }
    
    
  }
}