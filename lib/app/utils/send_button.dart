import 'package:flutter/material.dart';

final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
    elevation: 5.0,
    foregroundColor: const Color.fromARGB(255, 83, 68, 21),
    backgroundColor: Colors.amber,
    padding: const EdgeInsets.symmetric(horizontal: 30),
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)));
